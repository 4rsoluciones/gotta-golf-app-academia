angular.module('app')

.run(function($ionicPlatform, $rootScope, $ocLazyLoad, $ionicHistory, $q, $state, $injector, $versionChecker, $cordovaStatusbar, $cordovaToast) {

  $ionicPlatform.ready(function () {

    if (window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);

      window.addEventListener('native.keyboardshow', function(){
        document.body.classList.add('keyboard-open');
      });
    }
    // if (window.StatusBar) {
    //   StatusBar.styleDefault();
    // }

    $cordovaStatusbar.overlaysWebView(false);
    $cordovaStatusbar.styleHex("#09ccbe");

    $ionicPlatform.registerBackButtonAction(function(event){
      event.preventDefault();
      if($state.current.name == "newReport"){
        $ionicHistory.clearHistory();
        $state.go('tabs.reportList');
      }else{
        if($state.current.name == "login"){
          ionic.Platform.exitApp();
        }else {
          if ($state.current.name != "academyList") {
            $ionicHistory.goBack();
          }
        }
      }
    },100);

    $rootScope.compare = function(param1, param2){
      return angular.equals(param1, param2);
    };

    //En caso de no registrar el pago y estar logueado, se va a selección de academia
    $rootScope.$on('noPlanPayment', function (event, data) {
      var userData = window.localStorage['userData'];
      if(userData){
        $cordovaToast.show('No se registró el pago del plan de la academia. Contáctese con el responsable de la misma.', 'long', 'bottom');
        $state.go('academyList');
      }
    });

    //En caso de que lo desvinculen de una academia
    $rootScope.$on('noAcademyMembership', function (event, data) {
      $state.go('academyList');
    });

    //Carga de modulos lazy load
    var promises = [];
    promises.push($ocLazyLoad.load('components/webServices/main.js'));
    promises.push($ocLazyLoad.load('components/common/main.js'));
    promises.push($ocLazyLoad.load('components/push/main.js'));
    promises.push($ocLazyLoad.load('components/login/main.js'));
    promises.push($ocLazyLoad.load('components/academy/main.js'));
    promises.push($ocLazyLoad.load('components/onboarding/main.js'));
    promises.push($ocLazyLoad.load('components/tabs/main.js'));
    promises.push($ocLazyLoad.load('components/youtube/main.js'));
    promises.push($ocLazyLoad.load('components/drive/main.js'));
    promises.push($ocLazyLoad.load('components/report/main.js'));
    promises.push($ocLazyLoad.load('components/routine/main.js'));
    promises.push($ocLazyLoad.load('components/material/main.js'));
    promises.push($ocLazyLoad.load('components/student/main.js'));
    promises.push($ocLazyLoad.load('components/changePass/main.js'));
    promises.push($ocLazyLoad.load('components/media/main.js'));
    promises.push($ocLazyLoad.load('components/contact/main.js'));
    promises.push($ocLazyLoad.load('components/services/main.js'));

    $q.all(promises).then(function () {

      //setea URL de store
      $versionChecker.config({
        android: 'com.gea.ag.app',
        ios: 'id1287476259'
      });

      var userData = window.localStorage['userData'];

      if(userData){

        try {
          $rootScope.userData = JSON.parse(userData || '{}');
        }catch(e){
          $rootScope.userData = {};
        }

        //inicializa notificaciones push
        var $pushFactory = $injector.get('$pushFactory');
        $pushFactory.init();

        try {
          if ($rootScope.userData.roles.indexOf('ROLE_TEACHER') !== -1)
            $rootScope.rol = 'teacher';
          else
            $rootScope.rol = 'student';
        }catch(e){
          $rootScope.rol = 'student';
        }

        var $webServices = $injector.get('$webServices');
        var $dataHandler = $injector.get('$dataHandler');

        $webServices.login().then(function(res) {
          window.localStorage['userData'] = JSON.stringify(res.user);
          $rootScope.userData = res.user;

        }, null).finally(function(){
          // $dataHandler.updateData().then(function(res){
          //   if(res.redirect == 'reportList')
          //     $state.go('tabs.reportList');
          //   else
              $state.go('academyList');
          // });
        });

      }else
        $state.go('login');
    }, function (error) {
      console.log('ERROR al cargar los modulos: ' + error);
    });

    $rootScope.goTo = function(state){
      $state.go(state);
    };

    $rootScope.goBack = function(){
      $ionicHistory.goBack();
    };

  });

})


