angular.module('ngVersionChecker', [])

  .run(function($ionicPlatform, $versionChecker, $rootScope, $cordovaAppVersion){

    $ionicPlatform.ready(function() {

      $versionChecker.init();

      $cordovaAppVersion.getVersionNumber().then(
        function (version) {
          $rootScope.appVersion = version;
        }
      )

    })
  })

  .factory('httpVersionInterceptor', function($q, $rootScope){

    return {
      'request': function(config){
        angular.extend(config.headers, {'X-APP-VERSION': $rootScope.appVersion});
        return config;
      },

      'requestError': function(rejection){
        return $q.reject(rejection);
      },

      'response': function(response){
        return response;
      },

      'responseError': function(rejection){
        return $q.reject(rejection);
      }
    }
  })

  .factory('$versionChecker', function($ionicModal, $rootScope){

    var storeConfig = {};

    return {
      init: function () {

        $ionicModal.fromTemplateUrl('lib/angularVersionChecker/versionModal.html', {
          scope: $rootScope,
          animation: 'fade-in',
          hardwareBackButtonClose: false
        }).then(function (modal) {
          $rootScope.modal = modal;
        });

        $rootScope.openModalVersion = function () {
          $rootScope.modal.show();
        };

        $rootScope.closeModalVersion = function () {
          $rootScope.modal.hide();
        };

        $rootScope.goToStore = function () {
          if(ionic.Platform.isAndroid() || ionic.Platform.device().platform == "Android")
            cordova.plugins.market.open(storeConfig.android);
          else
            cordova.plugins.market.open(storeConfig.ios);
        };

        $rootScope.$on('InvalidVersionDetected', function (event, data) {
          $rootScope.openModalVersion();
        });
      },

      config: function(store){
        storeConfig = store;
      }
    }
  })

;
