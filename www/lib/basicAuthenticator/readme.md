# ANGULAR BASIC AUTHENTICATION

Para usar la librería de autenticación BASIC, se deben incluir los siguientes archivos en la carpeta *www/lib/basicAuthenticator/* del proyecto en cuestión:

- angularBase64.js
- ngBasic.js
- authenticatorFiles.js
- ngAccessControl.js

Para instalar mediante **bower**, incluir en el archivo de configuración *bower.json* lo siguiente:


``` 
{
...
  "devDependencies": {
    ...
    "basicAuthenticator": "https://MyUser@bitbucket.org/4rsoluciones/ionic-authentication.git"
  }
}
```

y luego ejecutar por consola, situado en el directorio raíz del proyecto:


``` 
bower install
```
	
	
Para utilizar la libreria HTTP Basic, se deben efectuar los siguientes pasos:

- Incluir la librería en el módulo angular del proyecto (*ngBasicAuthenticator*):
    
	
``` 
angular.module('myModule', ['ionic','ngBasicAuthenticator'])
```
	
	
- Incluir el archivo *authenticatorFiles.js* en el index.html:


``` 
<script src="lib/basicAuthenticator/authenticatorFiles.js"></script>
```
	
	
- Incluir en el módulo de configuración el tipo de autenticación que se desea utilizar (*httpBasicInterceptor*):

    
```
.config(function($httpProvider){
	$httpProvider.interceptors.push('httpBasicInterceptor');
})
```

	
- Definir una variable de configuración con el siguiente formato:
    
	
```
.value('$apiConfig',
  {	  
	'api': [
	 {'url': 'http://example.com'}
	],
	'accessControl': [
	  {'url': '/api/login', 'auth': 'public'},
	  {'url': '/api/register', 'auth': 'public'},
	  {'url': '/api/recover_pass', 'auth': 'public'},
	],
	'defaultAuth': 'basic'
  }
)
``` 
    
	Donde se definen una o más URLs de APIs disponibles y los tipos de autenticación en cada web services (los que no se detallan explícitamente, toman el valor defaultAuth).

- Por ultimo, al momento de invocar la acción *login*, ya sea en el controlador o en el servicio (antes de la llamada http), setear los valores de **usuario** y **contraseña** en localStorage con los siguientes nombres:
    
	
```
window.localStorage['username'] = params.username;
window.localStorage['password'] = params.password;
```