angular.module('ngAccessControl', [])

.factory('$accessControl', function($apiConfig){
  return {
    getAuthType: function(url){

      if(this.isApiRequest(url)){
        //revisa si la URL tiene un permiso especifico
        for(var i=0; i < $apiConfig.accessControl.length; i++){
          if(url.indexOf($apiConfig.accessControl[i].url) !== -1){
            return $apiConfig.accessControl[i].auth;
          }
        }

        //asigna permiso por defecto
        return $apiConfig.defaultAuth;
      }else
        return 'public';
    },

    isApiRequest: function(url){
      for(var i=0; i < $apiConfig.api.length; i++){
        if(url.indexOf($apiConfig.api[i].url) !== -1){
          return true;
        }
      }
      return false;
    },

  }
})
