angular.module('app.services')

  .controller('ServicesCtrl', function($scope, $ionicPlatform, $ionicHistory, $cordovaInAppBrowser, $webServices, $apiConfig, $cordovaToast){

    $ionicPlatform.ready(function () {

      $scope.goBack = function(){
        $ionicHistory.goBack();
      };

      $scope.$on("$ionicView.beforeEnter", function(event, data){
        $webServices.getServices()
          .then(function(services) {
            $scope.services = assembleServices(services['success']);
          }, function(error) {
            $cordovaToast.show(error.text, 'long', 'bottom');
          })
      });

    });

    function assembleServices(platforms) {
      var services = {};
      $scope.platforms = [];
      var platformKeys = Object.keys(platforms);
      $scope.platforms = platformKeys;
      for (var i = 0; i < platformKeys.length; i++) {
        var key = platformKeys[i];
        var platform = platforms[key];
        services[key] = [];
        for (var j = 0; j < platform.length; j++) {
          var service = {
            id: platform[j].id,
            name: platform[j].name
          };
          services[key].push(service);
        }
      }
      return services;
    }

    $scope.serviceClicked = function (service, platform) {
      open(getServiceUrl(service, platform));
    };

    function getServiceUrl(service, platform) {
      return getCenterUrl() + '/services/' + service.id + '/' + platform.split(' ').join('').toLowerCase();
    }

    function open(url) {
      $cordovaInAppBrowser.open(url, '_system', {});
    }

    function getCenterUrl() {
      return $apiConfig.api[1].url;
    }

  });
