angular.module('app.services', [[
  'components/services/controllers/servicesCtrl.js',
  'components/services/css/style.css'
]])

  .config(function ($stateProvider) {

    $stateProvider
      .state('services', {
        url: '/services',
        cache: false,
        templateUrl: 'components/services/templates/services.html',
        controller: 'ServicesCtrl'
      })

  });
