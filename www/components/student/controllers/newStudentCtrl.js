angular.module('app.student')

  .controller('NewStudentCtrl', function ($scope, $rootScope, $ionicPlatform, $state, $ionicLoading, $cordovaToast, $webServices) {

    $ionicPlatform.ready(function () {

      $scope.user = {};
      $scope.validation = {};

      $scope.createStudent = function () {

        if (!$scope.user.firstName) {
          $cordovaToast.show("Ingrese el nombre", 'long', 'bottom');
          return false;
        }
        if (!$scope.user.lastName) {
          $cordovaToast.show("Ingrese el apellido", 'long', 'bottom');
          return false;
        }

        $ionicLoading.show();
        $webServices.addStudent({
          'email': $scope.user.email,
          'firstName': $scope.user.firstName,
          'lastName': $scope.user.lastName
        }).then(function (res) {

          $ionicLoading.hide();
          $state.go('tabs.studentList');

        }, function (err) {
          $ionicLoading.hide();
          $cordovaToast.show(err.text, 'long', 'bottom');
        })
      };

      $scope.checkStudent = function () {
        if (!$scope.user.email) {
          $cordovaToast.show("Ingrese el email", 'long', 'bottom');
          return false;
        }

        $scope.validation.checked = false;

        $ionicLoading.show();
        $webServices.checkStudent({
          'email': $scope.user.email
        }).then(function (res) {
          try{
            $scope.user.firstName = res.profile.first_name;
            $scope.user.lastName = res.profile.last_name;
            $scope.validation.exists = true;
          }catch(e){
            $scope.user.firstName = null;
            $scope.user.lastName = null;
            $scope.validation.exists = false;
          }

          $scope.validation.checked = true;
          $ionicLoading.hide();
        }, function (err) {
          $ionicLoading.hide();
          $cordovaToast.show(err.text, 'long', 'bottom');
        })
      };

      $scope.goBack = function () {
        $state.go('tabs.studentList');
      }

    });

  });
