angular.module('app.student')

  .controller('studentListCtrl', function($scope, $rootScope, $state, $ionicPlatform, $timeout, $q, $filter, $stateParams, $webServices){

    $ionicPlatform.ready(function () {

      $scope.$on("$ionicView.beforeEnter", function(event, data){
        $rootScope.tabActive = 2;
        if(!$stateParams.noreload)
          $rootScope.$broadcast('resetStudentSearching');
        else
          $scope.search = {keyword: $stateParams.keyword};
      });

      $scope.studentList = [];
      $scope.loading = false;
      $scope.moreData = true;

      var limit = 1000;
      var offset = 0;

      /************* INICIALIZAR LISTADO *************/

      $scope.loadMore = function() {
        if(!$scope.loading) {
          getStudents().then(null, null).finally(function () {
            $timeout(function () {
              $scope.$broadcast('scroll.infiniteScrollComplete');
            }, 500)
          });
        }
      }

      function getStudents(){
        var d = $q.defer();

        $scope.loading = true;

        if($stateParams.noreload){
          $scope.studentList = JSON.parse(window.localStorage['studentList']);
          $scope.moreData = false;
          d.resolve();
          $scope.loading = false;
        }else{
          $webServices.getStudents({
            limit: limit,
            offset: offset
          }).then(function (res) {

            if (res && res.students) {
              $scope.studentList = res.students;

              //guarda en local storage
              window.localStorage['studentList'] = JSON.stringify(res.students);

            } else
              $scope.studentList = [];

            $scope.moreData = false;
            d.resolve();
            $scope.loading = false;
          }, function (err) {
            $scope.moreData = false;
            $scope.loading = false;
            d.reject();
          })
        }

        return d.promise;
      }

      /***************************** SELECCION DE ESTUDIANTE ******************************/

      $scope.selectedStudent = null;

      $scope.selectStudent = function(id){
        if($scope.selectedStudent == id)
          $scope.selectedStudent = null;
        else
          $scope.selectedStudent = id;
      };

      $scope.viewStudent = function(student, isReport){
        if(isReport) {
          $rootScope.$broadcast('Student', {type: 'Informes', name: $filter('getUsername')(student), id: student.id});
          $state.go('tabs.reportList');
        }else{
          $rootScope.$broadcast('Student', {type: 'Rutinas', name: $filter('getUsername')(student), id: student.id});
          $state.go('tabs.routineList');
        }
      }

      /******************************** BUSCAR ESTUDIANTE ********************************/

      $scope.$on('searchStudent', function(event, data){
        $scope.search = {keyword: data.keyword};
      });

    })

  });
