angular.module('app.drive')

  .factory('$driveApi', function ($http, $q, $timeout, $cordovaInAppBrowser, $rootScope, $cordovaFileTransfer, $cordovaFile) {

    //url autorización
    var urlOauth2 = 'https://accounts.google.com/o/oauth2/auth';

    //permisos
    var scopeUrl = 'https://www.googleapis.com/auth/drive';

    //url para solicitar un token
    var urlGetToken = 'https://accounts.google.com/o/oauth2/token';

    //url que genera una solicitud para subir un archivo
    var urlResumable = 'https://www.googleapis.com/upload/drive/v3/files?uploadType=resumable';

    //datos de configuracion
    var configData = {};

    var getCodeFromUrl = function (url) {
      var regex = /[?&]([^=#]+)=([^&#]*)/g, match;
      while (match = regex.exec(url)) {
        if (match[1] == 'code') return match[2];
      }
      return null;
    };

    var getAccessCode = function () {
      console.log('configData',JSON.stringify(configData));
      var d = $q.defer(), code, browser;

      browser = $cordovaInAppBrowser.open(
        urlOauth2 + '?' +
        'client_id=' + configData.clientId +
        '&response_type=code' +
        '&scope=' + scopeUrl +
        '&redirect_uri=' + configData.redirectUri +
        '&access_type=offline',
        '_blank', {});

      //autorizó la api
      $rootScope.$on('$cordovaInAppBrowser:loadstart', function (e, event) {
        code = getCodeFromUrl(event.url);
        if (code) {
          if (browser) $cordovaInAppBrowser.close();
          d.resolve(code);
        }
      });

      //cierre intencional del navegador
      $rootScope.$on('$cordovaInAppBrowser:exit',
        function () {
          if (!code) d.reject();
          else d.resolve(code);
        });

      return d.promise;
    };

    //obtener token de acceso -> dura una hora y luego hay que renovarlo
    var getAccessToken = function (code) {
      var d = $q.defer();

      $http({
        method: 'POST',
        url: urlGetToken,
        data: 'code=' + code + '&' + 'client_id=' + configData.clientId + '&' + 'client_secret=' + configData.clientSecret + '&' + 'redirect_uri=' + configData.redirectUri + '&grant_type=authorization_code',
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded'
        }
      }).then(
        function (res) {
          res.data.token_request = new Date().getTime(); //guardo la fecha en milisegundos
          console.log('GET ACCESS TOKEN',JSON.stringify(res.data));
          window.localStorage['DRIVE_DATA'] = JSON.stringify(res.data);
          d.resolve();
        },
        function (err) {
          console.log(JSON.stringify(err));
          d.reject();
        }
      );

      return d.promise;
    };

    //recuperar access_token al expirar el mismo
    var refreshToken = function () {
      var driveTokenData = JSON.parse(window.localStorage['DRIVE_DATA']);
      var d = $q.defer();
      $http({
        method: 'POST',
        url: urlGetToken,
        data: 'client_id=' + configData.clientId + '&' + 'client_secret=' + configData.clientSecret + '&' + 'refresh_token=' + driveTokenData.refresh_token + '&' + 'grant_type=refresh_token',
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded'
        }
      }).then(
        function (res) {
          driveTokenData.access_token = res.data.access_token;
          driveTokenData.expires_in = res.data.expires_in;
          driveTokenData.token_type = res.data.token_type;
          driveTokenData.token_request = new Date().getTime(); //guardo la fecha en milisegundos
          window.localStorage['DRIVE_DATA'] = JSON.stringify(driveTokenData);
          d.resolve();
        },
        function (err) {
          console.error('ERROR REFRESH TOKEN' ,JSON.stringify(err));
          d.reject();
        }
      );

      return d.promise;
    };

    var verifyToken = function () {
      var driveTokenData = JSON.parse(window.localStorage['DRIVE_DATA']);
      var now = new Date().getTime();
      var diff_dates = now - driveTokenData.token_request;

      return (diff_dates < (driveTokenData.expires_in * 1000));
    };

    var getFileData = function (url, method) {
      var d = $q.defer();

      var directory = '';
      var fileName = url;

      if(method == 'getFromRecorder') {
        fileName = url.split('/')[url.split('/').length - 1];
        directory = cordova.file.dataDirectory;
      }

      $cordovaFile.checkFile(directory, fileName).then(function (res) {
        res.file(function (file) {
          d.resolve(file);
        });
      }, function (err) {
        console.log(err);
        d.reject();
      });
      return d.promise;
    };


    return {

      setConfig: function(config){
        console.log('config',config);
        if(config){
          configData = config;
        }
      },

      getCredentials: function () {
        var d = $q.defer();
        if (!window.localStorage.getItem('DRIVE_DATA')) { //primera vez que intenta acceder a la api
          console.log('getAccessCode');
          getAccessCode().then(
            function (code) {
              getAccessToken(code).then(
                function () {
                  d.resolve();
                },
                function () {
                  d.reject();
                });
            },
            function () {
              d.reject();
            }
          );
        }
        else {
          if (verifyToken()) {
            d.resolve();
          }
          else {
            refreshToken().then(
              function () {
                d.resolve();
              },
              function () {
                d.reject();
              }
            );
          }
        }

        return d.promise;
      },

      //funcion para subir video
      uploadFile: function (file, method) {
        var driveTokenData = JSON.parse(window.localStorage['DRIVE_DATA']);
        var d = $q.defer();
        var urlWithUploadId = null;

        getFileData(file.location, method).then(
          async function (res) {
            file.name = res.name;
            file.type = res.type;

            var options = {
              fileKey: 'file',
              httpMethod: 'PUT',
              fileName: file.name,
              mimeType: file.type,
              timeout: 600000,
              chunkedMode: true,
              headers: {
                'Authorization': 'Bearer ' + driveTokenData.access_token,
                'Content-Type': file.type
              }
            };
          

            $http({
              method: 'POST',
              url: urlResumable,
              headers: {
                Authorization: 'Bearer ' + driveTokenData.access_token
              },
              data: {
                kind: "drive#file",
                name: file.title,
                parents: file.folderId,
                // description: file.description,
                mimeType: file.type
              }
            }).then(
              function (res) {
                urlWithUploadId = res.headers().location;
                $cordovaFileTransfer.upload(urlWithUploadId, file.location, options)
                  .then(
                    function (res) {

                      var id = null;
                      try{
                        id = JSON.parse(res.response).id;
                      }catch(e){}
                      d.resolve({id: id, item: file.item});
                    },
                    function (err) {
                      console.log('primer error',JSON.stringify(err));
                      d.reject(err);
                    },
                    function (progress) {
                      progress = angular.extend(progress, {item: file.item});
                      d.notify(progress);
                    });
              },
              function (err) {
                console.log('segundo error',JSON.stringify(err));
                d.reject();
              }
            );
          },
          function () {
            d.reject();
          }
        );

        return d.promise;
      }
    };

  });
