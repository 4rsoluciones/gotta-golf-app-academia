angular.module('app.webServices')

.service('$dataHandler', function($rootScope, $q, $webServices) {

  var _this = this;

  _this.getStudents = getStudents;
  _this.getAcademies = getAcademies;
  _this.updateData = updateData;
  _this.checkAcademyPayment = checkAcademyPayment;

  /**
   * Funcion para actualizar listado de estudiantes en localStorage
   * @returns {Promise}
   */
  function getStudents(){

    var d = $q.defer();

    $webServices.getStudents({
      limit: 10000,
      offset: 0
    }).then(function(res) {
      if (res && res.students)
        window.localStorage['studentList'] = JSON.stringify(res.students);
      d.resolve();
    }, function(err){
      d.reject(err);
    });

    return d.promise;
  }

  /**
   * Funcion para actualizar listado de academias en localStorage
   * @params: rol (teacher / student)
   * @returns {Promise}
   */
  function getAcademies(){

    var d = $q.defer();

    $webServices[$rootScope.rol == 'teacher' ? 'getTeacherAcademies' : 'getStudentAcademies']({
      limit: 10000,
      offset: 0
    }).then(function(res) {
      if (res && res.academies)
        window.localStorage['academyList'] = JSON.stringify(res.academies);
      d.resolve();
    }, function(err){
      d.reject(err);
    });

    return d.promise;
  }

  /**
   * Funcion para actualizar informacion al ingresar a la app
   * @returns {Promise}
   */
  function updateData(){

    var d = $q.defer();

    //sincronizar data
    _this.getAcademies().then(null,null).finally(function(){
      //se actualizaron las academias, entonces se revisa si ya tiene una seleccionada para obtener los estudiantes
      //de la misma y actualizar los datos de la academia seleccionada
      if(window.localStorage['academy']) {
        try {
          var academy = JSON.parse(window.localStorage['academy'] || '{}');
          var academyList = JSON.parse(window.localStorage['academyList'] || '[]');

          for (var i = 0; i < academyList.length; i++) {
            if (academyList[i].id == academy.id) {
              window.localStorage['academy'] = JSON.stringify(academyList[i]);
            }
          }

          //todo: enviar a academia siempre
          d.resolve({redirect: 'academyList'});

          //actualiza lista de estudiantes
          // _this.getStudents().then(function(){
          //   d.resolve({redirect: 'reportList'});
          // }, function(err){
          //   console.log(err);
          //   //si el error fue por conexion a internet va a reporte
          //   if(err.status != 402)
          //     d.resolve({redirect: 'reportList'});
          //   else
          //     d.resolve({redirect: 'academyList'});
          //   //TODO: ver de colocar reject para que no haga nada, porq lo hace el listener de 402
          // })

        } catch (e) {
          console.log(e);
          d.resolve({redirect: 'academyList'});
        }
      }else
        d.resolve({redirect: 'academyList'});
    });

    return d.promise;
  }

  /**
   * Funcion para saber si se pago el plan de la academia (siendo estudiante)
   */
  function checkAcademyPayment(){

    var d = $q.defer();

    $webServices.getGoals({
      limit: 5,
      offset: 0
    }).then(function(res) {
      d.resolve();
    }, function(err){
      d.reject(err);
    });

    return d.promise;
  }

});


