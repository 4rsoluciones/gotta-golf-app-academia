angular.module('app.webServices')

  .factory('$webServices', function ($rootScope, $q, $http, $apiConfig) {

    function getCenterUrl() {
      return $apiConfig.api[1].url;
    }

    return {

      /**
       *   Funcion para efectuar una peticion HTTP personalizada, manejando promesas
       *   y retornando resultados segun su respuesta
       */
      http: function (config) {
        var d = $q.defer();
        console.log(JSON.stringify(config.data))
        $http({
          method: config.method,
          url: (config.api ? config.api : $apiConfig.api[0].url) + config.url,
          params: config.params,
          data: config.data,
          headers: config.headers,
          timeout: (config.timeout ? config.timeout : 40000)
        }).then(function (res) {
          console.log('[Web Service] HTTP OK ' + config.method + ' URL ' + config.api  + config.url);
          d.resolve(res.data);
        }, function (err) {
          console.log('[Web Service] HTTP ERROR ' + config.method + ' URL ' + config.api + config.url + ' : ' + JSON.stringify(err));

          switch (err.status) {
            case -1:
              d.reject({
                'error': 'timeout',
                'text': 'Verifique su conexión a internet e inténtelo nuevamente.',
                'status': -1
              });
              break;
            case 400:
              try {
                d.reject({
                  'error': err.data.errors.children,
                  'text': 'Error de validación',
                  'status': 400
                });
              } catch (e) {
                d.reject({
                  'error': 'error',
                  'text': 'Error de validación',
                  'status': 400
                });
              }
              break;
            case 401:
              d.reject({
                'error': 'unauthorized',
                'text': 'Su usuario o contraseña es incorrecto',
                'status': 401
              });
              break;
            case 402:
              try {
                console.log(err);
                if (err.data.message.indexOf('Academy student limit reached.') !== -1) {
                  d.reject({
                    'error': 'limit_reached',
                    'text': 'Ha alcanzado el límite máximo de estudiantes por academia.',
                    'status': 402
                  });
                } else {
                  $rootScope.$broadcast('noPlanPayment');
                  d.reject({
                    'error': 'plan_payment',
                    'text': 'No se registró el pago del plan de la academia. Contactese con el responsable de la misma.',
                    'status': 402
                  });
                }
              } catch (e) {
                console.log(e);
                $rootScope.$broadcast('noPlanPayment');
                d.reject({
                  'error': 'plan_payment',
                  'text': 'No se registró el pago del plan de la academia. Contáctese con el responsable de la misma.',
                  'status': 402
                });
              }
              break;
            case 403:
              try {
                if (err.data.message.indexOf('Academy membership not found') !== -1) {
                  $rootScope.$broadcast('noAcademyMembership');
                  d.reject({
                    'error': 'noAcademyMembership',
                    'text': 'Has sido desvinculado de la academia.',
                    'status': 403
                  });
                } else {
                  d.reject({
                    'error': 'error',
                    'text': 'Ha ocurrido un error. Por favor, inténtelo nuevamente.',
                    'status': 403
                  });
                }
              } catch (e) {
                console.log(e);
                $rootScope.$broadcast('noAcademyMembership');
                d.reject({
                  'error': 'noAcademyMembership',
                  'text': 'Has sido desvinculado de la academia.',
                  'status': 403
                });
              }
              break;
            case 418:
              $rootScope.$broadcast('InvalidVersionDetected');
              d.reject({
                'error': 'old version',
                'text': 'La versión de su aplicación esta desactualizada.',
                'status': 418
              });
              break;
            case 500:
              d.reject({
                'error': 'timeout',
                'text': 'Ha ocurrido un error. Por favor, inténtelo nuevamente.',
                'status': 500
              });
              break;
            default:
              d.reject({
                'error': 'error',
                'text': 'Ha ocurrido un error. Por favor, inténtelo nuevamente.'
              });
          }
        });
        return d.promise;
      },


      login: function () {
        return this.http({
          method: 'GET',
          api: $apiConfig.api[0].url,
          url: '/api/login'
        })
      },

      requestRecoverPass: function (params) {
        return this.http({
          method: 'GET',
          api: $apiConfig.api[0].url,
          url: '/api/recover/request',
          params: params
        })
      },

      recoveryPass: function (params) {
        return this.http({
          method: 'POST',
          api: $apiConfig.api[0].url,
          url: '/api/recover/process',
          data: params
        })
      },

      changePass: function (params) {
        return this.http({
          method: 'GET',
          api: $apiConfig.api[0].url,
          url: '/api/credentials',
          params: params
        })
      },

      getLessons: function (params) {
        return this.http({
          method: 'GET',
          api: $apiConfig.api[0].url,
          url: '/api/' + $rootScope.rol + '/lessons',
          params: params
        })
      },

      getStudentLessons: function (params) {
        return this.http({
          method: 'GET',
          api: $apiConfig.api[0].url,
          url: '/api/teacher/student/' + params.id + '/lessons',
          params: {
            limit: params.limit,
            offset: params.offset
          }
        })
      },

      getRoutines: function (params) {
        return this.http({
          method: 'GET',
          api: $apiConfig.api[0].url,
          url: '/api/' + $rootScope.rol + '/routines',
          params: params
        })
      },

      getStudentRoutines: function (params) {
        return this.http({
          method: 'GET',
          api: $apiConfig.api[0].url,
          url: '/api/teacher/student/' + params.id + '/routines',
          params: {
            limit: params.limit,
            offset: params.offset
          }
        })
      },

      getIndications: function (params) {
        return this.http({
          method: 'GET',
          api: $apiConfig.api[0].url,
          url: '/api/' + $rootScope.rol + '/indications',
          params: params
        })
      },

      getGoals: function (params) {
        return this.http({
          method: 'GET',
          api: $apiConfig.api[0].url,
          url: '/api/' + $rootScope.rol + '/goals',
          params: params
        })
      },

      getAccessToken: function () {
        return this.http({
          method: 'GET',
          api: $apiConfig.api[0].url,
          url: '/api/teacher/access_tokens'
        })
      },

      addReport: function (params) {
        return this.http({
          method: 'POST',
          api: $apiConfig.api[0].url,
          url: '/api/teacher/lesson/add ',
          data: params
        })
      },

      addRoutine: function (params) {
        return this.http({
          method: 'POST',
          api: $apiConfig.api[0].url,
          url: '/api/teacher/routine/add',
          data: params
        })
      },

      addIndication: function (params) {
        console.log('add indication');
        return this.http({
          method: 'POST',
          api: $apiConfig.api[0].url,
          url: '/api/teacher/indication/add',
          data: params
        })
      },

      editIndication: function (id, params) {
        return this.http({
          method: 'PUT',
          api: $apiConfig.api[0].url,
          url: '/api/teacher/indication/edit/' + id,
          data: params
        })
      },

      addGoal: function (params) {
        return this.http({
          method: 'POST',
          api: $apiConfig.api[0].url,
          url: '/api/teacher/goal/add',
          data: params
        })
      },

      addReportFeedback: function (params) {
        return this.http({
          method: 'POST',
          api: $apiConfig.api[0].url,
          url: '/api/student/lesson/feedback/add',
          data: params
        })
      },

      addRoutineFeedback: function (params) {
        return this.http({
          method: 'POST',
          api: $apiConfig.api[0].url,
          url: '/api/student/routine/feedback/add',
          data: params
        })
      },

      setStepCompleted: function (params) {
        return this.http({
          method: 'PUT',
          api: $apiConfig.api[0].url,
          url: '/api/student/routine/step',
          data: params
        })
      },

      addStudent: function (params) {
        return this.http({
          method: 'GET',
          api: $apiConfig.api[0].url,
          url: '/api/teacher/student/create-assign',
          params: params
        })
      },

      checkStudent: function (params) {
        return this.http({
          method: 'GET',
          api: $apiConfig.api[0].url,
          url: '/api/teacher/user/email',
          params: params
        })
      },

      getStudents: function (params) {
        return this.http({
          method: 'GET',
          api: $apiConfig.api[0].url,
          url: '/api/teacher/students',
          params: params
        })
      },

      registerPush: function (params) {
        return this.http({
          method: 'POST',
          api: $apiConfig.api[0].url,
          url: '/api/push/register-device',
          data: params
        })
      },

      editReport: function (id, params) {
        return this.http({
          method: 'PUT',
          api: $apiConfig.api[0].url,
          url: '/api/teacher/lesson/edit/' + id,
          data: params
        })
      },

      deleteReport: function (params) {
        return this.http({
          method: 'DELETE',
          api: $apiConfig.api[0].url,
          url: '/api/teacher/lesson/remove/' + params.id
        })
      },

      editRoutine: function (id, params) {
        return this.http({
          method: 'PUT',
          api: $apiConfig.api[0].url,
          url: '/api/teacher/routine/edit/' + id,
          data: params
        })
      },

      deleteRoutine: function (params) {
        return this.http({
          method: 'DELETE',
          api: $apiConfig.api[0].url,
          url: '/api/teacher/routine/remove/' + params.id
        })
      },

      deleteMedia: function (params) {
        return this.http({
          method: 'DELETE',
          api: $apiConfig.api[0].url,
          url: '/api/teacher/indication/remove/' + params.id
        })
      },

      getTeacherAcademies: function () {
        return this.http({
          method: 'GET',
          api: $apiConfig.api[0].url,
          url: '/api/teaches-in-academies'
        })
      },

      getStudentAcademies: function () {
        return this.http({
          method: 'GET',
          api: $apiConfig.api[0].url,
          url: '/api/enrolled-in-academies'
        })
      },

      getServices: function () {
        return this.http({
          method: 'GET',
          api: getCenterUrl(),
          url: '/api/services/by-platform'
        })
      }

    }

  });


