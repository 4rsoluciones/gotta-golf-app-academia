angular.module('app.contact', [[
  'components/contact/controllers/contactCtrl.js',
  'components/contact/css/style.css'
]])

  .config(function ($stateProvider) {

    $stateProvider
      .state('contact', {
        url: '/contact',
        cache: false,
        templateUrl: 'components/contact/templates/contact.html',
        controller: 'ContactCtrl'
      })

  });
