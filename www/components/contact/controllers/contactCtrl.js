angular.module('app.contact')

  .controller('ContactCtrl', function($scope, $ionicPlatform, $ionicHistory, $cordovaInAppBrowser){

    $ionicPlatform.ready(function () {

      $scope.goBack = function(){
        $ionicHistory.goBack();
      };

      $scope.open = function(url){
        $cordovaInAppBrowser.open(url, '_system', {});
      };

    })

  });
