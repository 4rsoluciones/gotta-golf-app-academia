angular.module('app.academy')

  .controller('AcademyListCtrl', function ($scope, $rootScope, $ionicPlatform, $timeout, $state, $pushFactory, $ionicPopover, $dataHandler, $ionicLoading, $cordovaToast) {

    $ionicPlatform.ready(function () {

      var popOptions = null;

      $timeout(function () {
        navigator.splashscreen.hide();
      }, 1000);

      $scope.flagPlatform = ionic.Platform.isAndroid() || ionic.Platform.device().platform == "Android";

      // $scope.academyList = JSON.parse(window.localStorage['academyList'] || '[]');

      $scope.connectionError = false;

      $scope.$on("$ionicView.beforeEnter", function (event, data) {
        refresh();
      });

      $scope.select = function (academy) {

        window.localStorage['academy'] = JSON.stringify(academy);

        if ($rootScope.rol == 'teacher') {
          $ionicLoading.show();
          $dataHandler.getStudents().then(function () {
            $ionicLoading.hide();
            redirect();
          }, function (err) {
            $ionicLoading.hide();
            if (err.status != 402) {
              redirect();
            }
          })
        } else {
          $ionicLoading.show();
          $dataHandler.checkAcademyPayment().then(function () {
            $ionicLoading.hide();
            redirect();
          }, function (err) {
            $ionicLoading.hide();
            if (err.status != 402) {
              redirect();
            }
          })
        }
      };

      function redirect() {
        $rootScope.$broadcast('ReportListReload');
        $rootScope.$broadcast('reloadMaterial');
        $rootScope.$broadcast('ReloadAcademy');
        $state.go('tabs.reportList');
      }


      /************************** MENU DESPLEGABLE ***********************************/
      popOptions = $ionicPopover.fromTemplate('<div class="menu-custom"><div class="item" ng-click="optionSelected(\'refresh\')">Actualizar</div><div class="item" ng-click="optionSelected(\'contact\')">Contacto</div><div class="item" ng-click="optionSelected(\'services\')">Servicios AG</div><div class="item" ng-click="optionSelected(\'logout\')">Cerrar Sesión</div></div>', {
        scope: $scope
      });

      $scope.openOptions = function ($event) {
        popOptions.show($event);
      };

      $scope.optionSelected = function (option) {
        popOptions.hide();
        switch (option) {
          case 'refresh':
            refresh();
            break;
          case 'contact':
            $state.go('contact');
            break;
          case 'services':
            $state.go('services');
            break;
          case 'logout':
            logout();
            $state.go('login');
            break;
        }
      };

      $scope.refresh = function () {
        $ionicLoading.show();
        $timeout(function () {
          refresh();
        }, 1000);
      };

      function refresh() {
        $ionicLoading.show();
        $dataHandler.getAcademies().then(function () {
          $scope.academyList = JSON.parse(window.localStorage['academyList'] || '[]');
          $scope.connectionError = false;
          $ionicLoading.hide();
        }, function (err) {
          if (err.status == -1) {
            $scope.connectionError = true;
          } else {
            $scope.connectionError = false;
            if (err.status != 401)
              $cordovaToast.show('No se pudo obtener el listado de academias.', 'long', 'bottom');
          }

          if (err.status == 401) {
            $cordovaToast.show('Ha ocurrido un error de autenticación. Vuelva a iniciar sesión.', 'long', 'bottom');
            logout();
            $state.go('login');
          }

          $ionicLoading.hide();
        })
      }

      function logout() {
        $pushFactory.logout();
        window.localStorage.clear();
        $rootScope.userData = null;
        $rootScope.rol = null;
      }

    });

  });
