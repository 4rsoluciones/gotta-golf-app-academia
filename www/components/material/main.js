angular.module('app.material', [[
  'components/material/controllers/materialHeaderCtrl.js',
  'components/material/controllers/materialCtrl.js',
  'components/material/controllers/materialInfoCtrl.js',
  'components/material/css/style.css'
]])

  .config(function ($stateProvider) {

    $stateProvider
      .state('tabs.materialHeader', {
        url: '/materialHeader',
        cache: true,
        abstract: true,
        templateUrl: 'components/material/templates/header.html',
        controller: 'MaterialHeaderCtrl'
      })
      .state('tabs.materialHeader.video', {
        url: '/video',
        cache: true,
        templateUrl: 'components/material/templates/material.html',
        controller: 'MaterialCtrl',
        params:{
          type: 'video'
        }
      })
      .state('tabs.materialHeader.image', {
        url: '/image',
        cache: true,
        templateUrl: 'components/material/templates/material.html',
        controller: 'MaterialCtrl',
        params:{
          type: 'image'
        }
      })
      .state('tabs.materialHeader.sound', {
        url: '/sound',
        cache: true,
        templateUrl: 'components/material/templates/material.html',
        controller: 'MaterialCtrl',
        params:{
          type: 'sound'
        }

      })
      .state('materialInfo', {
        url: '/materialInfo',
        cache: false,
        templateUrl: 'components/material/templates/materialInfo.html',
        controller: 'MaterialInfoCtrl',
        params: {
          media: null,
          fromReport: null
        }
      })

  });
