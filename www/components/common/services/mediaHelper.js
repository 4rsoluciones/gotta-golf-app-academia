angular.module('app.common')

.factory('$mediaHelper', function($rootScope, $ionicPlatform, $ionicActionSheet, $ionicModal, $q, $cordovaCapture, $cordovaCamera, $state) {

    var isAndroid = ionic.Platform.isAndroid() || ionic.Platform.device().platform == "Android";
    $rootScope.flagStopRecord = true;

    function getFromCaptureVideo() {
        var options = { limit: 1, duration: 30 };
        $cordovaCapture.captureVideo(options).then(
            function(video) {
                $state.go('newMedia', { path: video[0].fullPath, type: 'video', fromHome: $rootScope.fromHome });
            },
            function(err) {
                console.log(err);
            });
    };

    function getFromVideoGallery() {
        $cordovaCamera.getPicture({
            sourceType: Camera.PictureSourceType.PHOTOLIBRARY,
            destinationType: Camera.DestinationType.FILE_URI,
            mediaType: Camera.MediaType.VIDEO,
            targetWidth: 600,
            targetHeight: 600,
            quality: 50
        }).then(function(video) {
            if (video.indexOf('file://') == -1 && isAndroid)
                video = 'file://' + video;

            $state.go('newMedia', { path: video, type: 'video', fromHome: $rootScope.fromHome });
        }, function(err) {
            console.log(err);
        });
    };

    function getFromCamera() {
        $cordovaCamera.getPicture({
            sourceType: Camera.PictureSourceType.CAMERA,
            destinationType: Camera.DestinationType.FILE_URI,
            targetWidth: 600,
            targetHeight: 600,
            quality: 50
        }).then(function(image) {
            $state.go('newMedia', { path: image, type: 'image', fromHome: $rootScope.fromHome });
        }, function(err) {
            console.log(err);
        });
    };

    function getFromGallery() {
        $cordovaCamera.getPicture({
            sourceType: Camera.PictureSourceType.PHOTOLIBRARY,
            destinationType: Camera.DestinationType.FILE_URI,
            mediaType: Camera.MediaType.PICTURE,
            targetWidth: 600,
            targetHeight: 600,
            quality: 50
        }).then(function(image) {
            console.log(image);
            $state.go('newMedia', { path: image, type: 'image', fromHome: $rootScope.fromHome });
        }, function(err) {
            console.log(err);
        });
    };

    if (isAndroid) {
        window.plugins.audioRecorderAPI.dopermissions(function(msg) {
            // success
            console.log('Audio permissions ok: ' + msg);
        }, function(msg) {
            // failed
            console.error("audioRecorderAPI: permission not granted");
            alert("The app needs access to your microphone to function.");
        });
    }

    var recordAudio = function() {
        $rootScope.flagStopRecord = false;
        window.plugins.audioRecorderAPI.record(null, null, 120); // record 120 seconds
    };

    var stopAudio = function() {
        var d = $q.defer();
        if ($rootScope.flagStopRecord == false) {
            $rootScope.flagStopRecord = true;
            window.plugins.audioRecorderAPI.stop(
                function(msg) {
                    d.resolve(msg);
                },
                function(msg) {
                    d.reject(msg);
                });
        } else {
            d.reject();
        }

        return d.promise;
    };

    $rootScope.getFromAudio = function() {
        if ($rootScope.isRecording) {
            $rootScope.saveAudio(true);
        } else {
            recordAudio();
            $rootScope.isRecording = true;
        }
    };

    $rootScope.saveAudio = function(flag) {
        if (flag) {
            stopAudio().then(
                function(dir) {
                    $rootScope.closeAudioModal();
                    $state.go('newMedia', { path: dir, type: 'sound', fromHome: $rootScope.fromHome });
                },
                function(err) {
                    console.log(err);
                    $rootScope.closeAudioModal();
                }
            );
        } else {
            stopAudio();
            $rootScope.closeAudioModal();
        }
    };

    var audioModal = null;
    $ionicModal.fromTemplateUrl('components/common/templates/recordSoundModal.html', {
        scope: $rootScope,
        animation: 'slide-in-up'
    }).then(function(modal) {
        audioModal = modal;
    });

    var showAudioPopup = function() {
        $rootScope.isRecording = false;
        audioModal.show();
    };

    $rootScope.closeAudioModal = function() {
        audioModal.hide()
    };

    return {

        popNewMedia: function(fromHome) {

            $rootScope.fromHome = fromHome;

            //botones para Android e iOS
            var buttons = [];
            if (isAndroid)
                buttons = [
                    { text: '<i class="icon ion-ios-camera"></i>Imagen desde cámara' },
                    { text: '<i class="icon ion-images"></i>Imagen desde galería' },
                    { text: '<i class="icon ion-ios-videocam"></i>Video desde cámara' },
                    { text: '<i class="icon ion-android-film"></i>Video desde galería' },
                    { text: '<i class="icon ion-android-microphone"></i>Audio' }
                ];
            else
                buttons = [
                    { text: 'Imagen desde cámara' },
                    { text: 'Imagen desde galería' },
                    { text: 'Video desde cámara' },
                    { text: 'Video desde galería' },
                    { text: 'Audio' }
                ];

            $ionicActionSheet.show({
                buttons: buttons,
                buttonClicked: function(index) {
                    switch (index) {
                        case 0:
                            getFromCamera();
                            break;
                        case 1:
                            getFromGallery();
                            break;
                        case 2:
                            getFromCaptureVideo();
                            break;
                        case 3:
                            getFromVideoGallery();
                            break;
                        case 4:
                            showAudioPopup();
                            break;
                    }
                    return true;
                }
            });
        },
    }

});