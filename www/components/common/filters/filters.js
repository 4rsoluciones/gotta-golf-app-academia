angular.module('app.common')

  .filter('setSportImage', function ($apiConfig) {

    return function (sport, type) {
      if (type == 'big') {
        return $apiConfig.api[0].url + '/img/icon/academy/big/' + sport;
      }

      if (type == 'small') {
        return $apiConfig.api[0].url + '/img/icon/academy/small/' + sport;
      }
    }

  })

  .filter('getUsername', function () {

    return function (user) {

      if (!user.profile || (!user.profile.first_name && !user.profile.last_name))
        return user.username;
      else
        return user.profile.first_name + ' ' + user.profile.last_name;
    }

  })

  .filter('getStudents', function () {

    return function (userArray, keyword) {

      if(!keyword)
        return userArray;

      if(!userArray)
        return [];

      var filtered = [];
      for (var i = 0; i<userArray.length; i++) {
        if (!userArray[i].profile || (!userArray[i].profile.first_name && !userArray[i].profile.last_name)) {
          if (userArray[i].username.toString().toLowerCase().indexOf(keyword.toLowerCase()) !== -1)
            filtered.push(userArray[i]);
        } else {
          if((userArray[i].profile.first_name + ' ' + userArray[i].profile.last_name).toString().toLowerCase().indexOf(keyword.toString().toLowerCase()) !== -1)
            filtered.push(userArray[i]);
        }
      }

      return filtered;
    }

  });
