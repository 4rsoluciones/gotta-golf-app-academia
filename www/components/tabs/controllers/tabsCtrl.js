angular.module('app.tabs')

.controller('TabsCtrl', function($scope, $rootScope, $state, $ionicPlatform, $mediaHelper, $ionicViewSwitcher, $ionicModal, $ionicPopover, $pushFactory, $timeout){

  $ionicPlatform.ready(function () {

    $scope.flagPlatform = ionic.Platform.isAndroid() || ionic.Platform.device().platform == "Android";
    $rootScope.tabActive = 0;
    var popOptions = null;

    $scope.searchStudent = {};
    $scope.searchRoutine = {};

    $scope.academy = JSON.parse(window.localStorage['academy'] || '{}');

    $rootScope.$on("ReloadAcademy", function(event, data){
      $scope.academy = JSON.parse(window.localStorage['academy'] || '{}');
    });

    /************************** FILTRO DE ESTUDIANTE ***********************************/

    $rootScope.$on("Student", function(event, data){
      $rootScope.studentFilter = data;
    });

    $scope.cancelFilter = function(){
      $rootScope.studentFilter = null;
      $state.go('tabs.studentList', {noreload: true, keyword: $scope.searchStudent.keyword});
    };


    /************************** BUSCAR RUTINA ***********************************/

    $scope.searchRoutineTapped = function () {
      if ($scope.searchRoutineFlag) {
        $rootScope.$broadcast('searchRoutine', {keyword: null});
        $scope.searchRoutine.keyword = null;
      } else {
        try {
          $timeout(function () {
            document.getElementById("tabRoutineInput").focus()
          }, 300)
        } catch (e) {
        }
      }
      $scope.searchRoutineFlag = $scope.searchRoutineFlag ? false : true;
    };

    $scope.routineSearchChange = function () {
      if ($scope.searchRoutine && $scope.searchRoutine.keyword && $scope.searchRoutine.keyword.length > 2)
        $rootScope.$broadcast('searchRoutine', {keyword: $scope.searchRoutine.keyword});
      else
        $rootScope.$broadcast('searchRoutine', {keyword: null});
    };

    $scope.$on('resetRoutineSearching', function (event, data) {
      $scope.searchRoutineFlag = false;
      $scope.searchRoutine.keyword = null;
    });


    /************************** BUSCAR ESTUDIANTE ***********************************/

    $scope.searchStudentTapped = function(){
      if($scope.searchStudentFlag){
        $rootScope.$broadcast('searchStudent', {keyword: null});
        $scope.searchStudent.keyword = null;
      }else {
        try {
          $timeout(function(){
            document.getElementById("tabStudentInput").focus()
          }, 300)
        }catch (e){}
      }
      $scope.searchStudentFlag = $scope.searchStudentFlag ? false : true;
    };

    $scope.studentSearchChange = function(){
      if($scope.searchStudent && $scope.searchStudent.keyword && $scope.searchStudent.keyword.length > 2)
        $rootScope.$broadcast('searchStudent', {keyword: $scope.searchStudent.keyword});
      else
        $rootScope.$broadcast('searchStudent', {keyword: null});
    };

    $scope.$on('resetStudentSearching', function(event, data){
      $scope.searchStudentFlag = false;
      $scope.searchStudent.keyword = null;
    });

    /************************** TAB ***********************************/

    $scope.createNew = function(){
      switch($rootScope.tabActive){
        case 0:
          $rootScope.$broadcast('reloadReport');
          $ionicViewSwitcher.nextDirection('forward');
          $state.go('newReport');
          break;
        case 1:
          $state.go('newRoutine');
          break;
        case 2:
          $state.go('newStudent');
          break;
        case 3:
          $mediaHelper.popNewMedia(true);
          break;
        default:
          console.log('hacer case default');
          break;
      }
    };

    /************************** MENU DESPLEGABLE ***********************************/

    $ionicPopover.fromTemplateUrl('components/common/templates/menu.html', {
      scope: $scope
    }).then(function(popover) {
      popOptions = popover;
    });

    $scope.openOptions = function($event) {
      popOptions.show($event);
    };

    $scope.optionSelected = function(option) {
      popOptions.hide();
      switch(option){
        case 'changeAcademy':
          $state.go('academyList');
          break;
        case 'changePassword':
          $state.go('changePass');
          break;
        case 'contact':
          $state.go('contact');
          break;
        case 'services':
          $state.go('services');
          break;
        case 'logout':
          logout();
          $state.go('login');
          break;
      }

    };

    function logout(){
      $pushFactory.logout();
      window.localStorage.clear();
      $rootScope.userData = null;
      $rootScope.rol = null;
    }

    /********************* SEARCH GOAL *********************/
    $scope.searching = false;
    $scope.search = {keyword: null};
    $scope.goal = null;

    $scope.searchTapped = function(flag){
      if($scope.searching && !flag) {
        $rootScope.$broadcast('cancelSearchTapped');
      }else{
        $rootScope.$broadcast('searchTapped');
      }
    };

    $scope.$on('cancelSearchTapped', function(event, data){
      $scope.searching = false;
    });

    $scope.$on('goalSearching', function(event, data){
      $scope.goal = data.goal;
      $scope.search.keyword = data.keyword;
      if(!data.goal && !data.keyword)
        $scope.searching = false;
      else
        $scope.searching = true;
    });

    /*********************** IR A ***************************/

    $scope.goToReportList = function(){
      $rootScope.$broadcast('ReportListReload');
      $state.go('tabs.reportList');
    };

    $scope.goToRoutineList = function(){
      $rootScope.$broadcast('RoutineListReload');
      $state.go('tabs.routineList');
    };

  })

});
