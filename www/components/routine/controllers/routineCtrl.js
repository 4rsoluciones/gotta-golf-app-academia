angular.module('app.routine')

  .controller('RoutineCtrl', function($scope, $rootScope, $ionicPlatform, $state, $ionicScrollDelegate, $ionicModal, $ionicLoading, $stateParams, $cordovaToast, $webServices){

    $ionicPlatform.ready(function () {

      $scope.routine = $stateParams.routine;

      $scope.expand = function(section){
        $scope.routine[section] = $scope.routine[section] ? false : true;
        $ionicScrollDelegate.resize();
      }

      var feedbackModal = null;

      $scope.openFeedback = function(value){
        $scope.feedback.value = value;
        feedbackModal.show();
      };

      $scope.sendFeedback = function() {

        if(!$scope.feedback.value){
          $cordovaToast.show("Ingrese su calificación", 'long', 'bottom');
          return
        }

        $ionicLoading.show();
        $webServices.addRoutineFeedback({
          feedback: {
            routine: $scope.routine.id,
            value: $scope.feedback.value,
            notes: $scope.feedback.notes
          }
        }).then(function(res){
          $scope.routine.feedback = $scope.feedback;
          $scope.routine.feedbackExpanded = true;
          $ionicLoading.hide();
          feedbackModal.hide();
        }, function(err){
          $ionicLoading.hide();
          $cordovaToast.show(err.text, 'long', 'bottom');
        })
      }

      $scope.closeFeedbackModal = function(){
        feedbackModal.hide();
      };

      $scope.feedback = {};
      $ionicModal.fromTemplateUrl('components/common/templates/feedbackModal.html', {
        scope: $scope,
        animation: 'slide-in-up'
      }).then(function(modal){
        feedbackModal = modal;
      });

      var stepChange = false;
      $scope.setCompletedStep = function(step, index) {
        if ($rootScope.rol == 'student' && !step.loading) {
          step.loading = true;
          $webServices.setStepCompleted({
            routine_step_status:{
              routine: $scope.routine.id,
              step: index,
              completed: step.completed ? 0 : 1
            }
          }).then(function(res){
            step.loading = false;
            stepChange = true;
            step.completed = step.completed ? false : true;
          }, function(err){
            step.loading = false;
          });
        }
      };

      $scope.goBack = function(){
        if(stepChange)
          $rootScope.$broadcast('RoutineListReload');
        $state.go('tabs.routineList');
      }

    })

  });
