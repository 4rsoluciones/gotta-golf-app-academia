angular.module('app.routine')

  .controller('RoutineListCtrl', function($scope, $rootScope, $ionicPlatform, $state, $ionicViewSwitcher, $ionicScrollDelegate, $ionicPopover, $timeout, $webServices, $q, $cordovaToast, $ionicLoading, $ionicPopup){

    $ionicPlatform.ready(function () {

      var routineSelected = null;
      var indexSelected = null;
      var popOptions = null;

      var limit = 10;
      var offset = 0;

      $scope.$on("$ionicView.beforeEnter", function(event, data){
        $rootScope.tabActive = 1;
        $rootScope.$broadcast('resetRoutineSearching');
      });

      $scope.routines = [];
      $scope.loading = false;
      $scope.moreData = true;
      $scope.hasPermission = true;

      var actualPromise = null;

      /************* INICIALIZAR LISTADO *************/

      $scope.loadMore = function() {
        if(!$scope.loading) {
          actualPromise = getRoutines().then(null, null).finally(function () {
            $timeout(function () {
              actualPromise = null;
              $scope.$broadcast('scroll.infiniteScrollComplete');
            }, 500)
          });
        }
      };

      function getRoutines(){
        var d = $q.defer();

        $scope.loading = true;

        $webServices[$rootScope.studentFilter ? 'getStudentRoutines' : 'getRoutines']({
          limit: limit,
          offset: offset,
          search: $scope.search.keyword ? $scope.search.keyword : '',
          id: $rootScope.studentFilter ? $rootScope.studentFilter.id : null
        }).then(function(res){

          if(res) {
            if(res.routines.length < limit)
              $scope.moreData = false;
            $scope.routines = [].concat($scope.routines, res.routines);
            offset += limit;
          }

          d.resolve();
          $scope.loading = false;
        }, function(err){
          if(err && err.error == 'timeout')
            $scope.moreData = false;

          d.reject();
          $scope.loading = false;
        });

        return d.promise;
      }

      function doSearch() {
        var d = $q.defer();

        $scope.loading = true;

        $webServices[$rootScope.studentFilter ? 'getStudentRoutines' : 'getRoutines']({
          limit: limit,
          offset: 0,
          search: $scope.search.keyword ? $scope.search.keyword : '',
          id: $rootScope.studentFilter ? $rootScope.studentFilter.id : null
        }).then(function(res){
          offset = 0;
          $scope.moreData = true;
          $scope.routines = [];

          if(res) {
            if(res.routines.length < limit)
              $scope.moreData = false;
            $scope.routines = [].concat($scope.routines, res.routines);
            offset += limit;
          }

          d.resolve();
          $scope.loading = false;
        }, function(err){
          if(err && err.error == 'timeout')
            $scope.moreData = false;

          d.reject();
          $scope.loading = false;
        });

        return d.promise;
      }

      /*************** FUNCIONES ******************/

      $scope.openRoutine = function(routine){
        $state.go('routine', {routine: routine});
      };

      $scope.clone = function(){
        popOptions.hide();
        $ionicViewSwitcher.nextDirection('forward');
        $state.go('newRoutine', {routine: routineSelected});
      };

      $scope.edit = function(){
        popOptions.hide();
        $ionicViewSwitcher.nextDirection('forward');
        $state.go('newRoutine', {routine: routineSelected, edit: true});
      };

      $scope.delete = function(){

        popOptions.hide();

        $ionicPopup.show({
          title: 'Confirmar',
          subTitle: '¿Desea eliminar el informe?',
          cssClass: 'confirm-pop',
          scope: $scope,
          buttons: [
            { text: 'Cancelar' },
            {
              text: 'Aceptar',
              onTap: function(e) {
                $ionicLoading.show();

                $webServices.deleteRoutine({'id': routineSelected.id}).then(function(){
                  $scope.routines.splice(indexSelected, 1);
                  $ionicLoading.hide();
                  $cordovaToast.show("Rutina eliminada exitosamente", 'long', 'bottom');
                }, function(err){
                  console.log(err);
                  $ionicLoading.hide();
                  $cordovaToast.show(err.text, 'long', 'bottom');
                })
              }
            }
          ]
        });

      };

      $ionicPopover.fromTemplateUrl('components/common/templates/optionPopOver.html', {
        scope: $scope
      }).then(function(popover) {
        popOptions = popover;
      });

      $scope.moreOptionPop = function($event, routine, index) {
        $scope.hasPermission = routine.teacher.id == $rootScope.userData.id;

        routineSelected = routine;
        indexSelected = index;
        popOptions.show($event);
      };


      /*************** FILTRO ESTUDIANTE ****************/

      $rootScope.$on("Student", function(event, data){
        $timeout(function(){
          reload();
        }, 100)
      });

      /********************** REINICIAR LISTADO *****************************/

      $rootScope.$on("RoutineListReload", function(event, data){
        $timeout(function(){
          reload();

          //parche por si se encuentra una promesa en accion
          // if(actualPromise)
          //   actualPromise.then(function(){
          //     reload();
          //   })
        }, 0)
      });

      function reload(){
        offset = 0;
        $scope.loading = false;
        $scope.routines = [];
        $scope.moreData = true;
        $ionicScrollDelegate.resize();
        $ionicScrollDelegate.scrollBottom();
      }

      /******************************** BUSCAR RUTINA ********************************/

      $scope.$on('searchRoutine', function(event, data){
        if ($scope.searchTimeout) {
          clearTimeout($scope.searchTimeout);
        }
        $scope.searchTimeout = setTimeout(function() {
          $scope.search = {keyword: data.keyword};
          doSearch();
        }, 500);
      });

    })

  });
