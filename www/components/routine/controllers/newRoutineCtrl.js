angular.module('app.routine')

  .controller('NewRoutineCtrl', function($scope, $rootScope, $ionicPlatform, $state, $ionicScrollDelegate, $ionicModal, $ionicLoading, $stateParams, $cordovaToast, $webServices){

    $ionicPlatform.ready(function () {

      function setCloneUser(routine){
        //setea rutina
        for(var i=0; i<$scope.routine.students.length; i++){
          if($scope.routine.students[i].id == routine.student.id)
            $scope.routine.students[i].selected = true;
        }

        //setea pasos
        $scope.routine.steps = [];
        for(var s=0; s<routine.steps.length; s++){
          $scope.routine.steps.push({
            title: routine.steps[s].title,
            description: routine.steps[s].description
          })
        }

        if(routine.id)
          $scope.routine.id = routine.id;

        //setea demas datos
        $scope.routine.title = routine.title;
        $scope.routine.resume = routine.resume;
      }

      var students = [];
      try {
        students = JSON.parse(window.localStorage['studentList'] || '[]');
      }catch(e){
        console.log(e);
      }

      $scope.context = $stateParams.edit;

      if($stateParams.routine){
        $scope.routine = {students: students};
        setCloneUser($stateParams.routine);
      }else {
        $scope.routine = {
          students: students,
          steps: [],
          expandStep: false
        };
      }

      var studentsModal = null;
      $ionicModal.fromTemplateUrl('components/common/templates/studentModal.html', {
        scope: $scope,
        animation: 'slide-in-up'
      }).then(function(modal) {
        studentsModal = modal;
      });

      var studentAux = null;

      $scope.openStudentModal = function(){
        console.log($scope.context);
        if(!$scope.context) {
          studentsModal.show();
          studentAux = angular.copy($scope.routine.students);
        }
      };

      $scope.cancelStudentChanges = function(){
        $scope.routine.students = angular.copy(studentAux);
        studentsModal.hide();
      };

      $scope.confirmStudentChange = function(){
        studentsModal.hide()
      };

      $scope.studentSelected = function(){
        for(var i=0; i<$scope.routine.students.length; i++){
          if($scope.routine.students[i].selected)
            return true;
        }
        return false;
      };

      $scope.createStep = function(){
        $scope.routine.steps.push({title: null, description: null})
      };

      $scope.expand = function(section){
        $scope.routine[section] = $scope.routine[section] ? false : true;
        $ionicScrollDelegate.resize();
      };

      $scope.sendRoutine = function(){

        if(!$scope.studentSelected()){
          $cordovaToast.show("Seleccione al menos un estudiante", 'long', 'bottom');
          return
        }
        if(!$scope.routine.title){
          $cordovaToast.show("Ingrese un título", 'long', 'bottom');
          return
        }
        if(!$scope.routine.resume){
          $cordovaToast.show("Ingrese la descripción", 'long', 'bottom');
          return
        }
        if(!checkStepsTitle()){
          $cordovaToast.show("Ingrese un título para cada paso de la rutina", 'long', 'bottom');
          return
        }

        $ionicLoading.show();

        if($scope.context) {
          $webServices.editRoutine($scope.routine.id, {
            add_routine: {
              title: $scope.routine.title,
              resume: $scope.routine.resume,
              steps: $scope.routine.steps
            }
          }).then(function (res) {
            $ionicLoading.hide();
            $cordovaToast.show("Rutina modificada correctamente", 'long', 'bottom');
            $rootScope.$broadcast('RoutineListReload');
            $state.go('tabs.routineList')
          }, function (err) {
            $ionicLoading.hide();
            $cordovaToast.show(err.text, 'long', 'bottom');
          });
        }else{
          $webServices.addRoutine({
            add_routine: {
              title: $scope.routine.title,
              resume: $scope.routine.resume,
              steps: $scope.routine.steps,
              students: getStudentsSelected()
            }
          }).then(function(res){
            $ionicLoading.hide();
            $cordovaToast.show("Rutina ingresada correctamente", 'long', 'bottom');
            $rootScope.$broadcast('RoutineListReload');
            $state.go('tabs.routineList')
          }, function(err){
            $ionicLoading.hide();
            $cordovaToast.show(err.text, 'long', 'bottom');
          });
        }
      };

      function getStudentsSelected(){
        var studentList = [];

        for(var i=0; i<$scope.routine.students.length; i++){
          if($scope.routine.students[i].selected)
            studentList.push($scope.routine.students[i].id)
        }

        return studentList;
      }

      function checkStepsTitle(){
        for(var i=0; i<$scope.routine.steps.length; i++){
          if(!$scope.routine.steps[i].title)
            return false;
        }

        if(!$scope.routine.steps.length)
          return false;

        return true;
      }

    })

  });
