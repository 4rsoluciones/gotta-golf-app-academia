angular.module('app.media')

  .controller('HeaderMediaCtrl', function($scope, $rootScope, $ionicPlatform, $mediaHelper, $state){

    $ionicPlatform.ready(function () {

      $scope.popNewMedia = function() {
        $mediaHelper.popNewMedia(false);
      };

      $scope.goBack = function() {
        $state.go('newReport');
      };

      /********************* SEARCH GOAL *********************/
      $scope.searching = false;
      $scope.search = {keyword: null};
      $scope.goal = null;

      $scope.searchTapped = function(flag){
        if($scope.searching && !flag) {
          $rootScope.$broadcast('cancelSearchTapped');
        }else{
          $rootScope.$broadcast('searchTapped');
        }
      };

      $scope.$on('cancelSearchTapped', function(event, data){
        $scope.searching = false;
      });

      $scope.$on('goalSearching', function(event, data){
        $scope.goal = data.goal;
        $scope.search.keyword = data.keyword;
        if(!data.goal && !data.keyword)
          $scope.searching = false;
        else
          $scope.searching = true;
      });

    });

  });
