angular.module('app.media')

  .controller('MediaCtrl', function($scope, $rootScope, $ionicPlatform, $state, $stateParams, $q, $webServices, $timeout, $goalHelper, $ionicScrollDelegate){

    $ionicPlatform.ready(function () {

      var type = null;
      var limit = 12;
      var offset = 0;
      var keyword = null;
      var goal = null;

      $scope.indicationsList = [];
      $scope.loading = false;
      $scope.moreData = true;

      var listener = null;
      $scope.$on('$ionicView.beforeLeave', function() {
        //al salir de la pantalla elimina el listener de modal objetivos
        listener();
      });

      $scope.$on("$ionicView.beforeEnter", function(event, data){
        //limpia filtro al cambiar de pantalla
        $rootScope.$broadcast('goalSearching', {goal: null, keyword: null});

        //inicializa modal de objetivos al entrar
        $goalHelper.init($scope);
        listener = $rootScope.$on('searchTapped', function(event, data){
          $goalHelper.show();
        });

        switch($stateParams.type) {
          case 'video':
            $rootScope.activeIndex = 0;
            type = 'type.video';
            break;
          case 'image':
            $rootScope.activeIndex = 1;
            type = 'type.image';
            break;
          case 'sound':
            $rootScope.activeIndex = 2;
            type = 'type.audio';
            break;
        }
      });

      $scope.$on('goalSearching', function(event, data){
        if(data.keyword || data.goal) {
          offset = 0;
          keyword = data.keyword;
          goal = data.goal && data.goal.id ? data.goal.id : null;
          $scope.indicationsList = [];
          $scope.loading = false;
          $scope.moreData = true;
          $ionicScrollDelegate.resize();
          $ionicScrollDelegate.scrollBottom();
        }
      });

      $scope.$on('cancelSearchTapped', function(event, data){
        offset = 0;
        keyword = null;
        goal = null;
        $scope.indicationsList = [];
        $scope.loading = false;
        $scope.moreData = true;
        $ionicScrollDelegate.resize();
        $ionicScrollDelegate.scrollBottom();
      });

      /************* INICIALIZAR LISTADO *************/

      $scope.loadMore = function() {
        if (!$scope.loading) {
          getIndications().then(null, null).finally(function () {
            $timeout(function () {
              $scope.$broadcast('scroll.infiniteScrollComplete');
            }, 0)
          });
        }
      }

      function getIndications(){
        var d = $q.defer();

        $scope.loading = true;

        $webServices.getIndications({
          limit: limit,
          offset: offset,
          type: type,
          goals: goal,
          query: keyword
        }).then(function(res){
          if(res) {
            if(res.indications.length < limit)
              $scope.moreData = false;
            $scope.indicationsList = [].concat($scope.indicationsList, res.indications);
            offset += limit;
          }

          d.resolve();
          $scope.loading = false;
        }, function(err){
          if(err && err.error == 'timeout')
            $scope.moreData = false;

          d.reject();
          $scope.loading = false;
        });

        return d.promise;
      }

      /**************************** FUNCIONES *****************************************/

      $scope.selectMedia = function(media){
        $rootScope.$broadcast('newMedia', {'media': media});
        $state.go('newReport');
      };

    })

  });
