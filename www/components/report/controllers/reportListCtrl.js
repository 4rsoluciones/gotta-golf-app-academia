angular.module('app.report')

  .controller('ReportListCtrl', function($scope, $rootScope, $ionicPlatform, $state, $q, $ionicPopover, $ionicViewSwitcher, $ionicScrollDelegate, $ionicPopup, $timeout, $webServices, $ionicLoading, $cordovaToast){

    $ionicPlatform.ready(function () {

      var reportSelected = null;
      var indexSelected = null;
      var popOptions = null;

      var limit = 10;
      var offset = 0;

      $timeout(function(){
        navigator.splashscreen.hide();
      }, 1000);

      $scope.$on("$ionicView.beforeEnter", function(event, data){
        $rootScope.tabActive = 0;
      });

      $rootScope.$on('InvalidVersionDetected', function (event, data) {
        $scope.moreData = false;
      });

      $scope.reportList = [];
      $scope.loading = false;
      $scope.moreData = true;
      $scope.hasPermission = true;

      /************* INICIALIZAR LISTADO *************/

      $scope.loadMore = function() {
        if(!$scope.loading) {
          getLessons().then(null, null).finally(function () {
            $timeout(function () {
              $scope.$broadcast('scroll.infiniteScrollComplete');
            }, 500)
          });
        }
      };

      function getLessons(){
        var d = $q.defer();

        $scope.loading = true;

        $webServices[$rootScope.studentFilter ? 'getStudentLessons' : 'getLessons']({
          limit: limit,
          offset: offset,
          id: $rootScope.studentFilter ? $rootScope.studentFilter.id : null
        }).then(function(res){

          if(res) {
            if(res.lessons.length < limit)
              $scope.moreData = false;
            $scope.reportList = [].concat($scope.reportList, res.lessons);
            offset += limit;
          }

          d.resolve();
          $scope.loading = false;
        }, function(err){

          console.log(err);

          if(err && err.error == 'timeout')
            $scope.moreData = false;

          d.reject();
          $scope.loading = false;
        })

        return d.promise;
      }

      /*************** FUNCIONES ******************/
      $scope.openReport = function(report){
        $state.go('report', {report: report});
      };

      $scope.edit = function(){
        popOptions.hide();
        $ionicViewSwitcher.nextDirection('forward');
        $state.go('newReport').then(function(){
          $rootScope.$broadcast('editReport', reportSelected);
        });
      };

      $scope.clone = function(){
        popOptions.hide();
        $ionicViewSwitcher.nextDirection('forward');
        $state.go('newReport').then(function(){
          $rootScope.$broadcast('reloadReport', reportSelected);
        });
      };

      $scope.delete = function(){
        popOptions.hide();
        // custom popup
        $ionicPopup.show({
          title: 'Confirmar',
          subTitle: '¿Desea eliminar el informe?',
          cssClass: 'confirm-pop',
          scope: $scope,
          buttons: [
            { text: 'Cancelar' },
            {
              text: 'Aceptar',
              onTap: function(e) {
                $ionicLoading.show();

                $webServices.deleteReport({'id': reportSelected.id}).then(function(){
                  $scope.reportList.splice(indexSelected, 1);
                  $ionicLoading.hide();
                  $cordovaToast.show("Informe eliminado exitosamente", 'long', 'bottom');
                }, function(err){
                  console.log(err);
                  $ionicLoading.hide();
                  $cordovaToast.show(err.text, 'long', 'bottom');
                })
              }
            }
          ]
        });
      };

      $ionicPopover.fromTemplateUrl('components/common/templates/optionPopOver.html', {
        scope: $scope
      }).then(function(popover) {
        popOptions = popover;
      });

      $scope.moreOptionPop = function($event, report, index) {
        $scope.hasPermission = report.teacher.id == $rootScope.userData.id;

        reportSelected = report;
        indexSelected = index;
        popOptions.show($event);
      };

      /********************** FILTRO POR ESTUDIANTE *****************************/

      $rootScope.$on("Student", function(event, data){
        $timeout(function(){
          reload();
        }, 100)
      });


      /********************** REINICIAR LISTADO *****************************/

      $rootScope.$on("ReportListReload", function(event, data){
        $timeout(function(){
          reload();
        }, 0)
      });

      function reload(){
        offset = 0;
        $scope.loading = false;
        $scope.reportList = [];
        $scope.moreData = true;
        $ionicScrollDelegate.resize();
        $ionicScrollDelegate.scrollBottom();
      }

    })

  });
