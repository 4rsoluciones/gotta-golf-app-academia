angular.module('app.report')

.controller('NewReportCtrl', function ($scope, $rootScope, $ionicPlatform, $timeout, $ionicModal, $ionicHistory, $ionicScrollDelegate, $ionicLoading, $state, $cordovaToast, $webServices, $ionicPopup){

  $ionicPlatform.ready(function () {

    var students = [];
    $scope.places = [];
    try {
      students = JSON.parse(window.localStorage['studentList'] || '[]');
      $scope.places = JSON.parse(window.localStorage['academy']  || '{}').academy_places;
    }catch(e){
      console.log(e);
    }

    $scope.report = {
      students: angular.copy(students),
      materialList: [],
      expandMaterial: false
    };

    $scope.context = null;

    $rootScope.$on("reloadReport", function(event, data){
      $scope.context = "publish";
      initReport(event, data);
    });

    $rootScope.$on("editReport", function(event, data){
      $scope.context = "edit";
      initReport(event, data);
    });

    function initReport(event, data){
      var students = [];
      $scope.places = [];
      try {
        students = JSON.parse(window.localStorage['studentList'] || '[]');
        $scope.places = JSON.parse(window.localStorage['academy']).academy_places;
      }catch(e){
        console.log(e);
      }

      if(data){
        $scope.report = {students: students} ;
        setCloneReport(data);
      }else{
        $scope.report = {
          students: angular.copy(students),
          materialList: [],
          expandMaterial: false
        };
      }

      $timeout(function(){
        $ionicScrollDelegate.resize();
        $ionicScrollDelegate.scrollTop();
      })
    }

    var studentsModal = null;
    $scope.openStudentModal = function(){
      if($scope.context != "edit") {
        studentsModal.show();
        studentAux = angular.copy($scope.report.students);
      }
    };

    $scope.cancelStudentChanges = function(){
      $scope.report.students = angular.copy(studentAux);
      studentsModal.hide();
    };

    $scope.confirmStudentChange = function(){
      studentsModal.hide()
    };

    $ionicModal.fromTemplateUrl('components/common/templates/studentModal.html', {
      scope: $scope,
      animation: 'slide-in-up'
    }).then(function(modal) {
      studentsModal = modal;
    });

    var studentAux = null;
    $scope.studentSelected = function(){
      for(var i=0; i<$scope.report.students.length; i++){
        if($scope.report.students[i].selected)
          return true;
      }
      return false;
    };

    $scope.expand = function(section){
      $scope.report[section] = $scope.report[section] ? false : true;
      $timeout(function() {
        $ionicScrollDelegate.resize();
      })
    };

    $rootScope.$on("newMedia", function(event, data){

      if(data.media && !existsMaterial(data.media)){
        $scope.report.materialList.push(data.media);
        $timeout(function(){
          $scope.report.expandMaterial = false;
          $timeout(function() {
            $ionicScrollDelegate.resize();
            $ionicScrollDelegate.scrollTop();
          })
        })
      }

    });

    $scope.sendReport = function() {

      if (!$scope.studentSelected()) {
        $cordovaToast.show("Seleccione al menos un estudiante", 'long', 'bottom');
        return
      }
      if (!$scope.report.date) {
        $cordovaToast.show("Ingrese una fecha", 'long', 'bottom');
        return
      }
      if (!$scope.report.place && $scope.report.place != 0) {
        $cordovaToast.show("Ingrese un lugar", 'long', 'bottom');
        return
      }
      if (!$scope.report.title) {
        $cordovaToast.show("Ingrese un título", 'long', 'bottom');
        return
      }
      if (!$scope.report.resume) {
        $cordovaToast.show("Ingrese la descripción", 'long', 'bottom');
        return
      }
      if (!$scope.report.materialList.length) {
        $cordovaToast.show("Ingrese al menos un material", 'long', 'bottom');
        return
      }

      $ionicLoading.show();

      if($scope.context == 'edit') {
        $webServices.editReport($scope.report.id, {
          lesson: {
            title: $scope.report.title,
            resume: $scope.report.resume,
            date: $scope.report.date,
            place: $scope.places[$scope.report.place].id,
            indications: getIndicationsSelected()
          }
        }).then(function (res) {
          $ionicLoading.hide();
          $cordovaToast.show("Informe modificado correctamente", 'long', 'bottom');

          $rootScope.$broadcast('ReportListReload');
          $state.go('tabs.reportList')
        }, function (err) {
          $ionicLoading.hide();
          $cordovaToast.show(err.text, 'long', 'bottom');
        });
      }else {
        $webServices.addReport({
          add_lesson: {
            title: $scope.report.title,
            resume: $scope.report.resume,
            date: $scope.report.date,
            place: $scope.places[$scope.report.place].id,
            indications: getIndicationsSelected(),
            students: getStudentsSelected()
          }
        }).then(function (res) {
          $ionicLoading.hide();
          $cordovaToast.show("Informe ingresado correctamente", 'long', 'bottom');

          $rootScope.$broadcast('ReportListReload');
          $state.go('tabs.reportList')
        }, function (err) {
          $ionicLoading.hide();
          $cordovaToast.show(err.text, 'long', 'bottom');
        });
      }
    };

    $scope.goBack = function(){
      $ionicHistory.clearHistory();
      $state.go('tabs.reportList');
    };

    $scope.goMaterial = function(){
      $state.go('headerMedia.video', {report: $scope.report});
    };

    $scope.deleteMedia = function(index){
      $ionicPopup.show({
        title: 'Confirmar',
        subTitle: '¿Desea quitar el material del informe?',
        cssClass: 'confirm-pop',
        scope: $scope,
        buttons: [
          { text: 'Cancelar' },
          {
            text: 'Aceptar',
            onTap: function(e) {
              $scope.report.materialList.splice(index, 1);
            }
          }
        ]
      });
    }

    function setCloneReport(report){
      //setea estudiante
      for(var i=0; i<$scope.report.students.length; i++){
        if($scope.report.students[i].id == report.student.id)
          $scope.report.students[i].selected = true;
      }

      //setea lugar
      for (var p=0; p<$scope.places.length; p++) {
        if ($scope.places[p].id == report.place.id)
          $scope.report.place = p;
      }

      //setea demas datos
      $scope.report.resume = report.report.resume;
      $scope.report.date = report.date;
      $scope.report.title = report.title;

      $scope.report.materialList = report.report.indications || [];

      if($scope.context == 'edit'){
        $scope.report.id = report.id;
      }
    }

    function getIndicationsSelected(){
      var materialList = [];

      for(var i=0; i<$scope.report.materialList.length; i++){
        materialList.push($scope.report.materialList[i].id)
      }

      return materialList;
    }

    function getStudentsSelected(){
      var studentList = [];

      for(var i=0; i<$scope.report.students.length; i++){
        if($scope.report.students[i].selected)
          studentList.push($scope.report.students[i].id)
      }

      return studentList;
    }

    function existsMaterial(media){
      for(var i=0; i<$scope.report.materialList.length; i++){
        if($scope.report.materialList[i].id == media.id)
          return true
      }
      return false;
    }
  });

});
