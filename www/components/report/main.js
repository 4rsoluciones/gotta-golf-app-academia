angular.module('app.report', [[
  'components/report/controllers/reportListCtrl.js',
  'components/report/controllers/newReportCtrl.js',
  'components/report/controllers/reportCtrl.js',
  'components/report/css/style.css'
]])

  .config(function ($stateProvider) {

    $stateProvider
      .state('tabs.reportList', {
        url: '/reportList',
        cache: true,
        templateUrl: 'components/report/templates/reportList.html',
        controller: 'ReportListCtrl'
      })

      .state('report', {
        url: '/report',
        cache: false,
        templateUrl: 'components/report/templates/report.html',
        controller: 'ReportCtrl',
        params: {
          report: null
        }
      })

      .state('newReport', {
        url: '/newReport',
        cache: true,
        templateUrl: 'components/report/templates/newReport.html',
        controller: 'NewReportCtrl'
      })

  });
