angular.module('app.changePass')

  .controller('ChangePassCtrl', function ($scope, $ionicPlatform, $ionicHistory, $ionicLoading, $cordovaToast, $rootScope, $webServices) {

    $ionicPlatform.ready(function () {

      $scope.user = [];

      $scope.saveChanges = function () {

        if (!$scope.user.oldPass) {
          $cordovaToast.show("Ingrese su contraseña actual", 'long', 'bottom');
          return false;
        }
        if (!$scope.user.newPass) {
          $cordovaToast.show("Ingrese su nueva contraseña", 'long', 'bottom');
          return false;
        }
        if($scope.user.newPass.length < 5){
          $cordovaToast.show("La contraseña debe tener al menos 5 caracteres", 'long', 'bottom');
          return false;
        }
        if (!$scope.user.newPass2) {
          $cordovaToast.show("Ingrese la confirmación de su contraseña", 'long', 'bottom');
          return false;
        }
        if ($scope.user.newPass != $scope.user.newPass2) {
          $cordovaToast.show("La contraseña ingresada no coincide con su confirmación", 'long', 'bottom');
          return false;
        }

        $ionicLoading.show();
        $webServices.changePass({
          'new_password': $scope.user.newPass,
          'old_password': $scope.user.oldPass
        }).then(function(res){

          window.localStorage['username'] = $rootScope.userData.username;
          window.localStorage['password'] = $scope.user.newPass;

          $ionicLoading.hide();
          $ionicHistory.goBack();

        }, function(err){
          $ionicLoading.hide();
          $cordovaToast.show(err.text, 'long', 'bottom');
        })

      }

    });

  });
