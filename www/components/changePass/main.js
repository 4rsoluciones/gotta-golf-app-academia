angular.module('app.changePass', [[
  'components/changePass/controllers/changePassCtrl.js'
]])

  .config(function ($stateProvider) {

    $stateProvider
      .state('changePass', {
        url: '/changePass',
        cache: false,
        templateUrl: 'components/changePass/templates/changePass.html',
        controller: 'ChangePassCtrl'
      })

  });
