angular.module('app.login')

  .controller('LoginCtrl', function ($scope, $rootScope, $ionicPlatform, $state, $ionicLoading, $ionicHistory, $pushFactory, $timeout, $cordovaToast, $webServices, $dataHandler, $cordovaInAppBrowser) {

    $ionicPlatform.ready(function () {

      $scope.user = [];
      $scope.step = 1;

      $ionicHistory.clearHistory();
      $ionicHistory.clearCache();

      $timeout(function () {
        navigator.splashscreen.hide();
      }, 1000);

      $scope.setStep = function (step) {
        $scope.step = -1;
        $timeout(function () {
          $scope.step = step;
        }, 500)
      };

      $scope.login = function () {

        if (!$scope.user.username) {
          $cordovaToast.show("Ingrese su email", 'long', 'bottom');
          return
        }
        if (!$scope.user.password) {
          $cordovaToast.show("Ingrese su contraseña", 'long', 'bottom');
          return
        }

        $ionicLoading.show();
        window.localStorage['username'] = $scope.user.username;
        window.localStorage['password'] = $scope.user.password;

        $webServices.login().then(function (res) {
          try {
            window.localStorage['userData'] = JSON.stringify(res.user);
            $rootScope.userData = res.user;

            $pushFactory.init();

            try {
              if ($rootScope.userData.roles.indexOf('ROLE_TEACHER') !== -1)
                $rootScope.rol = 'teacher';
              else
                $rootScope.rol = 'student';
            } catch (e) {
              $rootScope.rol = 'student';
            }

          } catch (e) {
            console.log(e);
          }

          $dataHandler.getAcademies().then(null, null).finally(function () {
            $ionicHistory.clearHistory();
            $ionicHistory.clearCache();
            $ionicLoading.hide();

            $state.go('onboarding');
          });

        }, function (err) {
          $ionicLoading.hide();
          $cordovaToast.show(err.text, 'long', 'bottom');
        })
      }

      $scope.recoveryPass = function () {

        if (!$scope.user.code) {
          $cordovaToast.show("Ingrese el código obtenido por email", 'long', 'bottom');
          return false;
        }
        if (!$scope.user.newPass) {
          $cordovaToast.show("Ingrese una contraseña", 'long', 'bottom');
          return false;
        }
        if ($scope.user.newPass.length < 5) {
          $cordovaToast.show("La contraseña debe tener al menos 5 caracteres", 'long', 'bottom');
          return false;
        }
        if (!$scope.user.newPass2) {
          $cordovaToast.show("Ingrese la confirmación de su contraseña", 'long', 'bottom');
          return false;
        }
        if ($scope.user.newPass != $scope.user.newPass2) {
          $cordovaToast.show("La contraseña ingresada no coincide con su confirmación", 'long', 'bottom');
          return false;
        }

        $ionicLoading.show();
        window.localStorage['username'] = $scope.user.username;
        window.localStorage['password'] = $scope.user.newPass;

        $webServices.recoveryPass({
          recover: {
            confirmationToken: $scope.user.code,
            plainPassword: $scope.user.newPass
          }
        }).then(function (res) {

          try {
            window.localStorage['userData'] = JSON.stringify(res.user);
            $rootScope.userData = res.user;

            $pushFactory.init();

            try {
              if ($rootScope.userData.roles.indexOf('ROLE_TEACHER') !== -1)
                $rootScope.rol = 'teacher';
              else
                $rootScope.rol = 'student';
            } catch (e) {
              $rootScope.rol = 'student';
            }

          } catch (e) {
            console.log(e);
          }

          $ionicHistory.clearHistory();
          $ionicHistory.clearCache();
          $ionicLoading.hide();

          $state.go('onboarding');

        }, function (err) {
          $ionicLoading.hide();
          if (err.status == 400)
            $cordovaToast.show("Codigo incorrecto", 'long', 'bottom');
          else
            $cordovaToast.show(err.text, 'long', 'bottom');
        })

      }

      $scope.getCode = function () {
        if (!$scope.user.username) {
          $cordovaToast.show("Ingrese su email", 'long', 'bottom');
          return;
        }

        $ionicLoading.show();
        $webServices.requestRecoverPass({email: $scope.user.username}).then(function (res) {
          $ionicLoading.hide();
          $cordovaToast.show("Se ha enviado un código a su email para recuperar su cuenta", 'long', 'bottom');
        }, function (err) {
          $ionicLoading.hide();
          $cordovaToast.show(err.text, 'long', 'bottom');
        })
      }

      $scope.goServices = function () {
        $cordovaInAppBrowser.open('http://sportsintouch.andresgotta.com.ar/', "_system", {});
      }

    });

  });
