angular.module('app.push', [[
  'components/push/services/pushFactory.js',
  'components/push/css/style.css'
]])

  .run(function ($state, $ionicPlatform, $cordovaPushV5, $pushConfig,  $rootScope, $ionicPopup) {

    var myPopup;

    $rootScope.$on('$cordovaPushV5:notificationReceived', function (event, data) {
      console.log('[Push Notification] Notification received: ' + JSON.stringify(data));

      if (myPopup)
        myPopup.close();

      myPopup = $ionicPopup.show({
        templateUrl: 'components/push/templates/popPush.html',
        cssClass: 'custom-pop-push',
        title: data.title,
        subTitle: data.message
      })
    });

    $rootScope.closePopPush = function () {
      myPopup.close();
      myPopup = null;
    }

  })
