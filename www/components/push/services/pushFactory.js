angular.module('app.push')

  .factory('$pushFactory', function ($q, $ionicPlatform, $cordovaPushV5, $pushConfig, $webServices) {

    return {

      init: function () {

        $ionicPlatform.ready(function () {

          $cordovaPushV5.initialize($pushConfig).then(function () {
            $cordovaPushV5.onNotification();
            $cordovaPushV5.onError();

            $cordovaPushV5.register().then(function (registrationId) {
              console.log('[Push Notification] Register OK id: ' + registrationId);

              if (registrationId != null && registrationId != undefined) {
                window.localStorage['pushId'] = registrationId;
                if (ionic.Platform.device().platform == 'Android' || ionic.Platform.isAndroid()) {
                  window.localStorage['deviceType'] = 'device_type.android.gcm';
                }
                if (ionic.Platform.platform() == 'ios') {
                  window.localStorage['deviceType'] = 'device_type.apple.apn';
                }
              }
            }).then(function () {
              $webServices.registerPush({
                'type': window.localStorage['deviceType'],
                'code': window.localStorage['pushId'],
              }).then(function () {
                console.log("[Push Notification] Register OK");
              }, function (error) {
                console.log("[Push Notification] Register ERROR: " + JSON.stringify(error));
              })
            })
          });
        });
      },

      logout: function(){
        $cordovaPushV5.unregister();
      }
    }

  });
