angular.module('app.onboarding', [['components/onboarding/controllers/onboardingCtrl.js', 'components/onboarding/css/style.css']])

  .config(function ($stateProvider) {

    $stateProvider

      .state('onboarding', {
        url: '/onboarding',
        cache: false,
        controller: 'OnboardingCtrl',
        templateUrl: 'components/onboarding/templates/onboarding.html',
        params: {
          last: false
        }
      })

  });
