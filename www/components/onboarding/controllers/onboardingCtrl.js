angular.module('app.onboarding')

  .controller('OnboardingCtrl', function ($state, $ionicLoading, $scope, $timeout, $ionicPlatform, $stateParams) {

    $ionicPlatform.ready(function () {

      if ($stateParams.last)
        $scope.options = {'initialSlide': 3};

      $scope.$on('$ionicSlides.sliderInitialized', function (event, data) {
        $scope.slider = data.slider;
      });

      $scope.$on("$ionicSlides.slideChangeStart", function (event, data) {
        $timeout(function () {
          $scope.slider = data.slider;
        })
      });

    })
  });
