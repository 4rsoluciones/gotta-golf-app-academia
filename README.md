# Sports in touch App


## Error: ERR_CLEAR_TEXT_NOT_PERMITTED:
Añadir en el manifest:
  <application
    ...
    android:usesCleartextTraffic="true"
    ...
  >
  
  </application>
   
## Error: ERR_CLEAR_TEXT_NOT_PERMITTED:
Añadir en el manifest:
  <application
    ...
    android:usesCleartextTraffic="true"
    ...
  >
  
  </application>
   
## Para preparar el entorno de trabajo:

```bash
npm install
```

### Compilar para android en mac
```bash
ionic cordova run android
```

En el archivo `platforms/android/project.properties` cambiar la version de play-services-gcm a la 11 si es necesario

### Compilar para ios en mac

Para development
```bash
ionic cordova run ios -- --buildFlag="-UseModernBuildSystem=0"
```

Para deploy
```bash
ionic cordova build --release ios -- --buildFlag="-UseModernBuildSystem=0"
```
Continuar proceso desde xcode

### Compilar con phonegap build
Tener en cuenta las configuraciones requeridas por phonegap.
Copiar el archivo `config.xml` y la carpeta `resources` en `www`